require 'rails_helper'

RSpec.describe User, type: :model do
  let(:client) { create(:user) }

  context 'validating required fields' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:message) }
    it { should validate_presence_of(:password) }
    it { should validate_presence_of(:email) }
  end

  it 'object valid with valid attributes' do
    user = build(:user)

    expect(user).to be_valid
  end

  it 'object invalid' do
    user = build(:user)
    user.name = nil

    expect(user).to_not be_valid
  end

  it 'new user always with role client' do
    user = build(:user)
    expect(user.client?).to eq(true)
  end

  context 'When create User' do
    let(:user) { build(:user) }
    it 'Before save to database with blank account' do
      expect(user.account).to be_blank
    end

    it 'After save to database with account present' do
      user.save
      expect(user.account).to be_present
    end
  end

  context 'Get all users by role' do
    let!(:manager_list) { create_list(:user, 3, :manager) }
    let!(:client_list) { create_list(:user, 2) }

    it 'using scope .client' do
      expect(User.client.count).to eq(client_list.count)
    end

    it 'using scope .mangager' do
      expect(User.manager.count).to eq(manager_list.count)
    end
  end

  context 'generate account number' do
    let!(:user) { build(:user) }

    it 'should create number' do
      user.save
      expect(user.account).to be_present
    end

    it 'should the account of being different' do
      client = create(:user)
      user.account = client.account
      user.save
      expect(user.account).to_not eq(client.account)
    end
  end
end