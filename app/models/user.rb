class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, #:registerable,
         :recoverable, :rememberable, :validatable

  validates :name, :message, presence: true

  enum role: {
    client: 0,
    manager: 1
  }

  before_create :generate_account

  def generate_account
    self.account = (SecureRandom.random_number * (10**6)).round.to_s
    generate_account if User.exists?(account: self.account)
  end
end
