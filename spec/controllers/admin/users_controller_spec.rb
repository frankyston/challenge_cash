# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Admin::UsersController, type: :controller do

  let!(:manager) { create(:user, :manager) }
  let!(:user) { create(:user) }

  describe 'sign in' do
    context 'should respond with success' do
      before(:each) do
        sign_in user
      end
      it 'redirect to root_url' do
        get :index
        expect(controller.after_sign_in_path_for(user)).to eq '/'
        expect(response).to have_http_status(:success)
      end
    end

    context 'should respond with success' do
      before(:each) do
        sign_in manager
      end
      it 'not redirect' do
        get :index
        expect(assigns(:users)).to eq([user])
        expect(controller.after_sign_in_path_for(manager)).to eq '/admin'
        expect(response).to have_http_status(:success)
      end
    end

  end
end
