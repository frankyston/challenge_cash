class ToastrNotify {
  constructor(title, subtitle, body) {
    this.title = title;
    this.subtitle = subtitle;
    this.body = body
  }

  alert(klass) {
    return {
      class: klass,
      title: this.title,
      subtitle: this.subtitle,
      body: this.body
    }
  }
}