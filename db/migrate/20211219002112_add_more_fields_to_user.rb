class AddMoreFieldsToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :account, :string
    add_index :users, :account, unique: true
    add_column :users, :name, :string
    add_column :users, :message, :text
    add_column :users, :role, :integer, default: 0
  end
end
