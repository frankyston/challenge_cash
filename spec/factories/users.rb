FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    name { Faker::Name.name }
    message { Faker::Lorem.paragraph }
    password { '123456' }
    trait :manager do
      role { 1 }
    end
  end
end
